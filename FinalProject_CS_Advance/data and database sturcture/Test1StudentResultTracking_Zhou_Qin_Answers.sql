-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: 159.203.41.95    Database: Test1StudentResultTracking_Zhou_Qin
-- ------------------------------------------------------
-- Server version	5.7.29-0ubuntu0.18.04.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Answers`
--

DROP TABLE IF EXISTS `Answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Answers` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `quizId` int(11) NOT NULL,
  `AnswerCollection` text,
  `result` double NOT NULL,
  `studentID` int(11) NOT NULL,
  `ReviewByParent` enum('Yes','No') NOT NULL DEFAULT 'No',
  PRIMARY KEY (`Id`),
  KEY `fk_quiz_test_idx` (`quizId`),
  KEY `fk_ans_stud` (`studentID`),
  CONSTRAINT `fk_ans_stud` FOREIGN KEY (`studentID`) REFERENCES `Students` (`Id`),
  CONSTRAINT `fk_ans_test` FOREIGN KEY (`quizId`) REFERENCES `Tests` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Answers`
--

LOCK TABLES `Answers` WRITE;
/*!40000 ALTER TABLE `Answers` DISABLE KEYS */;
INSERT INTO `Answers` VALUES (1,10,'4;2;2;2;2;2;2;2;2;3',30,1,'Yes'),(3,10,'4;4;2;3;4;4;3;3;3;3',40,4,'No'),(4,10,'4;4;4;3;3;3;3;3;3;3',30,11,'No'),(7,10,'3;2;2;2;2;2;2;3;4;4',30,5,'No'),(20,10,'3;2;2;2;2;2;2;3;4;4',60,2,'No'),(25,10,'3;2;2;2;2;2;2;3;4;4',49,3,'No'),(26,10,'3;2;2;2;2;2;2;3;4;4',90,6,'No'),(27,10,'3;2;2;2;2;2;2;3;4;4',80,8,'Yes'),(28,10,'3;2;2;2;2;2;2;3;4;4',80,9,'No'),(29,11,'4;4;3;3;3;1;1;1;2;2',40,11,'No'),(30,12,'4;3;3;3;2;1;1;1',25,11,'No'),(31,12,'4;1;2;1;3;1;3;3',100,2,'No'),(32,11,'3;1;1;4;3;1;2;3;2;4',70,5,'No'),(33,11,'4;1;2;3;2;1;2;3;3;4',70,8,'Yes'),(34,12,'4;1;3;1;3;1;3;4',75,2,'No'),(35,12,'4;1;2;1;3;1;3;3',100,10,'No'),(36,13,'2;1;2;2;4;3;1;2;3',77.78,1,'Yes'),(37,13,'1;1;2;3;4;2;1;2;3',88.89,3,'No'),(38,13,'1;2;2;2;3;2;2;2;3',66.67,4,'No'),(39,14,'1;4;1;1;2;1;2;3;4',100,6,'No'),(43,14,'1;3;2;1;2;1;3;3;4',66.67,9,'No'),(44,14,'2;4;1;1;3;1;2;3;3',66.67,10,'No'),(45,14,'1;3;2;2;1;2;2;3;4',44.44,11,'No'),(46,15,'3;2;2;2;4;3;3;3;3',66.67,2,'No'),(47,15,'3;2;2;3;4;3;2;3;3',88.89,10,'No'),(48,15,'3;2;2;2;4;1;2;2;3',66.67,3,'No'),(49,15,'1;2;2;3;4;3;1;2;3',55.56,4,'No'),(50,12,'4;1;2;1;1;1;2;3',75,8,'Yes'),(51,11,'3;1;1;2;4;1;2;1;2;4',70,5,'No'),(52,11,'4;1;1;3;4;1;2;3;2;4',100,10,'No'),(53,13,'1;1;3;3;4;2;1;2;3',77.78,6,'Yes'),(54,13,'2;1;2;3;4;3;1;2;3',66.67,9,'Yes'),(55,14,'1;4;1;2;2;1;2;3;4',88.89,1,'Yes'),(56,14,'1;4;1;1;2;1;2;3;4',100,5,'No'),(57,15,'3;2;3;3;4;4;1;4;3',66.67,6,'No'),(58,15,'2;1;2;3;4;4;2;3;3',77.78,11,'No'),(59,11,'4;1;1;3;4;1;2;3;2;4',100,2,'No'),(60,13,'4;2;2;3;2;2;2;2;2',33.33,11,'No'),(61,10,'4;2;2;2;2;2;2;2;2;2',30,12,'No'),(62,11,'4;4;2;3;3;1;2;3;2;4',70,12,'No'),(63,12,'4;1;2;1;3;1;3;3',100,12,'No'),(64,13,'1;1;2;2;4;2;1;2;3',100,12,'No'),(65,14,'3;4;1;1;2;2;2;2;2',55.55,12,'No'),(66,15,'4;4;3;3;2;2;2;3;3',44.44,12,'No'),(67,13,'2;1;2;2;4;2;1;2;3',88.89,2,'No'),(68,11,'4;4;4;4;4;4;4;4;4;0',20,9,'No'),(69,12,'3;3;3;3;3;3;3;3',37.5,9,'No'),(70,15,'3;3;2;2;2;3;3;3;0',33.33,9,'No'),(71,16,'4;2;2;0;0;0;0;0;0;0;0;0',16.67,11,'No');
/*!40000 ALTER TABLE `Answers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-14  7:39:09
