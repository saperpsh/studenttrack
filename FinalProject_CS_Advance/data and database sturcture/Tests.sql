-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 14, 2020 at 04:39 PM
-- Server version: 5.7.29-0ubuntu0.18.04.1-log
-- PHP Version: 7.2.24-0ubuntu0.18.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Test1StudentResultTracking_Zhou_Qin`
--

-- --------------------------------------------------------

--
-- Table structure for table `Tests`
--

CREATE TABLE `Tests` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `QuestionInTXT` text NOT NULL COMMENT 'question is format store in.\\\\\\\\\\\\\\\\n\\\\\\\\\\\\\\\\nquestion:xxxxxx;BBB;cccc;DDDD;EEEE;RightAnswer\\\\\\\\\\\\\\\\n\\\\\\\\nquestion is format store in.\\\\\\\\\\\\\\\\n\\\\\\\\\\\\\\\\nquestion:xxxxxx;BBB;cccc;DDDD;EEEE;RightAnswer\\\\\\\\\\\\\\\\n',
  `isAvailabeToStudent` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Tests`
--

INSERT INTO `Tests` (`id`, `date`, `QuestionInTXT`, `isAvailabeToStudent`) VALUES
(10, '2020-04-22', '65;+;24;4;95;80;16;89;\n31;+;24;1;55;90;67;104;\n35;+;32;1;67;27;131;63;\n49;+;9;3;3;31;58;49;\n76;+;19;4;118;103;171;95;\n48;+;5;1;53;11;17;27;\n16;+;23;2;7;39;27;24;\n25;+;71;3;101;18;96;114;\n38;+;49;2;10;87;9;69;\n72;+;1;4;121;58;107;73;\n', 1),
(11, '2020-04-22', '65;+;24;4;95;80;16;89;\n31;+;24;1;55;90;67;104;\n35;+;32;1;67;27;131;63;\n49;+;9;3;3;31;58;49;\n76;+;19;4;100;96;94;95;\n48;+;5;1;53;11;17;27;\n16;+;23;2;7;39;27;24;\n25;+;71;3;101;18;96;114;\n38;+;49;2;10;87;9;69;\n72;+;1;4;121;58;107;73;\n', 1),
(12, '2020-04-24', '12;+;22;4;22;6;25;34;\n69;+;5;1;74;19;116;34;\n42;+;3;2;79;45;70;53;\n16;+;40;1;56;5;22;99;\n19;+;29;3;9;38;48;54;\n80;-;2;1;78;91;86;66;\n40;*;2;3;146;33;80;65;\n28;/;4;3;10;4;7;10;\n', 1),
(13, '2020-04-30', '22;+;3;1;25;47;43;6;\n31;+;5;1;36;7;49;64;\n59;+;13;2;138;72;26;139;\n53;-;49;2;7;4;7;5;\n58;-;55;4;2;6;6;3;\n10;-;4;2;1;6;5;1;\n10;*;10;1;100;188;69;73;\n2;*;50;2;59;100;39;7;\n82;/;2;3;5;25;41;65;\n', 1),
(14, '2020-04-30', '14;+;56;1;70;5;117;19;\n73;+;14;4;47;31;134;87;\n88;+;3;1;91;49;152;88;\n15;-;2;1;13;3;6;24;\n17;-;10;2;2;7;11;8;\n80;-;76;1;4;6;3;3;\n21;*;4;2;49;84;114;136;\n39;*;2;3;22;116;78;117;\n48;/;6;4;13;1;1;8;\n', 1),
(15, '2020-04-30', '73;+;24;3;172;32;97;123;\n51;+;5;2;20;56;4;60;\n49;+;47;2;45;96;128;5;\n46;-;43;3;1;0;3;-1;\n73;-;14;4;36;62;86;59;\n41;-;32;4;8;7;13;9;\n8;*;12;2;78;96;94;70;\n16;*;6;3;33;80;96;20;\n27;/;3;3;13;13;9;3;\n', 1),
(16, '2020-04-30', '27;+;52;4;137;36;63;79;\n53;+;6;2;75;59;102;9;\n51;-;41;4;4;2;7;10;\n52;-;29;4;35;33;19;23;\n76;-;19;3;77;109;57;31;\n12;*;8;1;96;55;102;84;\n19;*;5;4;140;142;63;95;\n15;*;6;4;166;17;47;90;\n39;*;2;4;62;102;41;78;\n35;*;2;1;70;124;66;21;\n96;/;8;2;17;12;11;1;\n33;/;3;1;11;14;7;19;\n', 1),
(17, '2020-04-30', '70;+;16;4;119;42;68;86;\n69;+;20;2;136;89;98;31;\n49;+;24;4;89;112;12;73;\n77;-;41;3;21;24;36;26;\n76;-;3;3;128;10;73;40;\n6;*;16;3;87;34;96;136;\n9;*;11;1;99;98;6;131;\n85;/;5;3;24;14;17;11;\n10;/;2;4;9;2;9;5;\n', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Tests`
--
ALTER TABLE `Tests`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Tests`
--
ALTER TABLE `Tests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
