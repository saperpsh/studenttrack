-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 14, 2020 at 04:38 PM
-- Server version: 5.7.29-0ubuntu0.18.04.1-log
-- PHP Version: 7.2.24-0ubuntu0.18.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Test1StudentResultTracking_Zhou_Qin`
--

-- --------------------------------------------------------

--
-- Table structure for table `Parents`
--

CREATE TABLE `Parents` (
  `Id` int(11) NOT NULL,
  `LastName` varchar(45) NOT NULL,
  `FirstName` varchar(50) NOT NULL,
  `Phone` char(10) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `LoginPassword` varchar(20) NOT NULL,
  `loginID` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Parents`
--

INSERT INTO `Parents` (`Id`, `LastName`, `FirstName`, `Phone`, `Email`, `LoginPassword`, `loginID`) VALUES
(1, 'Hei', 'mao', '5174384562', 'marshal@gmail.com', 'HeiMao', 'heimao'),
(2, 'Ironman', 'Bill', '1239877412', 'iroman@gmail.com', 'IronmanB', 'ironman'),
(3, 'Superman', 'Butter', '7418529632', 'superman@gmail.com', 'SupermanB', 'superman'),
(4, 'Greenman', 'Will', '9638527411', 'greenman@gmail.com', 'GreenmanW', 'greenmaniii'),
(5, 'Hellokitty', 'Jape', '1234567899', 'hellokitty@gmail.com', 'HellokittyJ', 'hellokitty'),
(6, 'Lao', 'Yeye', '1748529632', 'shuwkong@gmail.com', 'laoyeye', 'laoyeye');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Parents`
--
ALTER TABLE `Parents`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `loginID_UNIQUE` (`loginID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Parents`
--
ALTER TABLE `Parents`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
