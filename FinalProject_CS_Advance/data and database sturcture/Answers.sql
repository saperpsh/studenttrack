-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 14, 2020 at 04:37 PM
-- Server version: 5.7.29-0ubuntu0.18.04.1-log
-- PHP Version: 7.2.24-0ubuntu0.18.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Test1StudentResultTracking_Zhou_Qin`
--

-- --------------------------------------------------------

--
-- Table structure for table `Answers`
--

CREATE TABLE `Answers` (
  `Id` int(11) NOT NULL,
  `quizId` int(11) NOT NULL,
  `AnswerCollection` text,
  `result` double NOT NULL,
  `studentID` int(11) NOT NULL,
  `ReviewByParent` enum('Yes','No') NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Answers`
--

INSERT INTO `Answers` (`Id`, `quizId`, `AnswerCollection`, `result`, `studentID`, `ReviewByParent`) VALUES
(1, 10, '4;2;2;2;2;2;2;2;2;3', 30, 1, 'Yes'),
(3, 10, '4;4;2;3;4;4;3;3;3;3', 40, 4, 'No'),
(4, 10, '4;4;4;3;3;3;3;3;3;3', 30, 11, 'No'),
(7, 10, '3;2;2;2;2;2;2;3;4;4', 30, 5, 'No'),
(20, 10, '3;2;2;2;2;2;2;3;4;4', 60, 2, 'No'),
(25, 10, '3;2;2;2;2;2;2;3;4;4', 49, 3, 'No'),
(26, 10, '3;2;2;2;2;2;2;3;4;4', 90, 6, 'No'),
(27, 10, '3;2;2;2;2;2;2;3;4;4', 80, 8, 'Yes'),
(28, 10, '3;2;2;2;2;2;2;3;4;4', 80, 9, 'No'),
(29, 11, '4;4;3;3;3;1;1;1;2;2', 40, 11, 'No'),
(30, 12, '4;3;3;3;2;1;1;1', 25, 11, 'No'),
(31, 12, '4;1;2;1;3;1;3;3', 100, 2, 'No'),
(32, 11, '3;1;1;4;3;1;2;3;2;4', 70, 5, 'No'),
(33, 11, '4;1;2;3;2;1;2;3;3;4', 70, 8, 'Yes'),
(34, 12, '4;1;3;1;3;1;3;4', 75, 2, 'No'),
(35, 12, '4;1;2;1;3;1;3;3', 100, 10, 'No'),
(36, 13, '2;1;2;2;4;3;1;2;3', 77.78, 1, 'Yes'),
(37, 13, '1;1;2;3;4;2;1;2;3', 88.89, 3, 'No'),
(38, 13, '1;2;2;2;3;2;2;2;3', 66.67, 4, 'No'),
(39, 14, '1;4;1;1;2;1;2;3;4', 100, 6, 'No'),
(43, 14, '1;3;2;1;2;1;3;3;4', 66.67, 9, 'No'),
(44, 14, '2;4;1;1;3;1;2;3;3', 66.67, 10, 'No'),
(45, 14, '1;3;2;2;1;2;2;3;4', 44.44, 11, 'No'),
(46, 15, '3;2;2;2;4;3;3;3;3', 66.67, 2, 'No'),
(47, 15, '3;2;2;3;4;3;2;3;3', 88.89, 10, 'No');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Answers`
--
ALTER TABLE `Answers`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `fk_quiz_test_idx` (`quizId`),
  ADD KEY `fk_ans_stud` (`studentID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Answers`
--
ALTER TABLE `Answers`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Answers`
--
ALTER TABLE `Answers`
  ADD CONSTRAINT `fk_ans_stud` FOREIGN KEY (`studentID`) REFERENCES `Students` (`Id`),
  ADD CONSTRAINT `fk_ans_test` FOREIGN KEY (`quizId`) REFERENCES `Tests` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
