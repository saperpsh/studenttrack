﻿using System;
using System.Linq;
using System.Windows;


namespace FinalProject_CS_Advance
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {

        static public Student currentStudent;
        static public Parent currentParent;
        static public Test1StudentResultTracking_Zhou_QinEntities2 ctx = new Test1StudentResultTracking_Zhou_QinEntities2();
        public LoginWindow()
        {

            InitializeComponent();
        }



        private void btExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btSubmit_Click(object sender, RoutedEventArgs e)
        {

            string user = CombLogin.SelectedValue.ToString();
            string password = pbPassword.Password;
            string loginId = tbCodePermanent.Text;
            checkValidateUser(user, loginId, password);

        }





        private void checkValidateUser(string user, string accountName, string password)
        {


            switch (user)
            {
                case "Teacher":

                    if (accountName == "Admin" && password == "Admin")
                    {
                        TeacherMainWindow teacherDialog = new TeacherMainWindow();
                        teacherDialog.ShowDialog();
                    }
                    else
                    {
                        lblInvalidInfor.Content = "Invalid username or password";
                        return;
                    }

                    break;

                case "Student":
                    var tempStudent = ctx.Students.FirstOrDefault(u => u.LoginID == accountName && u.LoginPassword == password);
                    if (tempStudent == null)
                    {
                        lblInvalidInfor.Content = "Invalid username or password";
                        return;
                    }

                    StudentMainWindow studentDialog = new StudentMainWindow(tempStudent);
                    studentDialog.ShowDialog();

                    break;
                case "Parent":

                    var tempParent = ctx.Parents.FirstOrDefault(u => u.loginID == accountName && u.LoginPassword == password);

                    if (tempParent == null)
                    {
                        lblInvalidInfor.Content = "Invalid username or password";
                        return;
                    }

                    ParentMainWindow parentDialog = new ParentMainWindow(tempParent);
                    parentDialog.ShowDialog();
                    break;
                default:
                    Console.WriteLine("Please select one!");
                    break;

            }

        }
    }
}
