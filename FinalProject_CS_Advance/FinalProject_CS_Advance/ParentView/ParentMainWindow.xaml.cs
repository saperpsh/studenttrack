﻿using EASendMail;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows;


namespace FinalProject_CS_Advance
{
    /// <summary>
    /// Interaction logic for ParentMainWindow.xaml
    /// </summary>
    public partial class ParentMainWindow : Window
    {

        Parent currentParent;
        List<Student> stdList;
        DrawGraphic drawline = new DrawGraphic();
        public ParentMainWindow(Parent parent)
        {
            currentParent = parent;
            stdList = stdgetAllKidBelongToParent(currentParent);
            InitializeComponent();
            drawAvgClassCurve();
        }

        private List<Student> stdgetAllKidBelongToParent(Parent current)
        {
            return (from std in LoginWindow.ctx.Students where current.Id == std.parentID select std).ToList();
        }

        void drawAvgClassCurve()
        {



            wpfPlot1.plt.XTicks(labels: DrawGraphic.quizIdString);
            wpfPlot1.plt.Legend(fixedLineWidth: false, location: ScottPlot.legendLocation.upperRight);
            wpfPlot1.plt.PlotScatter(DrawGraphic.dataX, DrawGraphic.dataY, color: Color.Magenta, label: "Class Average");

            wpfPlot1.Render();

        }

        string getStudentFirstName(int id)
        {
            return (from a in LoginWindow.ctx.Students where a.Id == id select a.FirstName).FirstOrDefault();
        }


        void drawIndividualCure(int studentId)
        {

            if (DrawGraphic.quizIdString.Length > 0)
            {
                double[] dataY = new double[DrawGraphic.quizIdString.Length];
                for (int i = 0; i < DrawGraphic.quizIdString.Length; i++)
                {
                    dataY[i] = DrawGraphic.getStdScoreByTestIdByQuizId(DrawGraphic.quizIdInt[i], studentId);
                }
                wpfPlot1.plt.XTicks(labels: DrawGraphic.quizIdString);
                wpfPlot1.plt.Legend(fixedLineWidth: false, fontSize: 6, location: ScottPlot.legendLocation.upperRight);
                wpfPlot1.plt.PlotBar(DrawGraphic.dataX, dataY,    showValues: true, label: getStudentFirstName(studentId), fill: false, outlineColor: Color.Blue);
                wpfPlot1.Render();
            }
        }


        private void btRDV_Click(object sender, RoutedEventArgs e)
        {


            try
            {
                SmtpMail oMail = new SmtpMail("TryIt");
                SmtpClient oSmtp = new SmtpClient();

                // Set sender email address, please change it to yours
                oMail.From = new MailAddress("test@emailarchitect.net");
                // Add recipient email address, please change it to yours
                oMail.To.Add(new MailAddress("support@emailarchitect.net"));

                // Set email subject and email body text
                oMail.Subject = "test email from C# XAML project";
                oMail.TextBody = "this is a test email sent from Windows Store App, do not reply";

                // Your SMTP server address
                SmtpServer oServer = new SmtpServer("smtp.emailarchitect.net");

                // User and password for SMTP authentication
                oServer.User = "test@emailarchitect.net";
                oServer.Password = "testpassword";

                // If your SMTP server requires TLS connection on 25 port, please add this line
                // oServer.ConnectType = SmtpConnectType.ConnectSSLAuto;

                // If your SMTP server requires SSL connection on 465 port, please add this line
                // oServer.Port = 465;
                // oServer.ConnectType = SmtpConnectType.ConnectSSLAuto;


                MessageBox.Show(this, "email was sent successfully!", "Email Send", MessageBoxButton.OK, MessageBoxImage.Information);

            }
            catch (Exception ep)
            {
                MessageBox.Show(this, "failed to send email with the following error:\n" + ep.Message, "Email Send", MessageBoxButton.OK, MessageBoxImage.Information);

            }
        }





        private void btAcknowledge_Click(object sender, RoutedEventArgs e)
        {
            var selectedAnswerList = lbQuizList.SelectedItems;

            int sudId = (int)cmbStudentId.SelectedItem;

            if (selectedAnswerList.Count > 0 && sudId > 0)
            {
                foreach (Answer ans in selectedAnswerList)
                {

                    if (ans.ReviewByParent == "No")
                    {
                        ans.ReviewByParent = "Yes";
                        LoginWindow.ctx.SaveChanges();
                    }
                }
                var lst = from t in LoginWindow.ctx.Answers where t.studentID == sudId select t;
                lbQuizList.ItemsSource = lst.ToList();
            }
        }

        private void cmbStudentId_Initialized(object sender, EventArgs e)
        {
            if (stdList.Count > 0)
                cmbStudentId.ItemsSource = stdList.Select(x => x.Id).ToList();
            cmbStudentId.SelectedIndex = -1;
            flag = false;

        }
        Boolean flag = true;
        private void cmbStudentId_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {

            if (flag)
                return;

            int sudId = (int)cmbStudentId.SelectedItem;
            if (sudId > 0)
            {

                var lst = from t in LoginWindow.ctx.Answers where t.studentID == sudId select t;

                if (lst.Count() > 0)
                {
                    lbQuizList.ItemsSource = lst.ToList();
                }
                else
                {
                    lbQuizList.ItemsSource = null;
                }


                wpfPlot1.Reset();
                drawIndividualCure(sudId);
                drawAvgClassCurve();
            }
        }
    }
}
