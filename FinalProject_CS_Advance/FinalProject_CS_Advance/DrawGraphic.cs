﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace FinalProject_CS_Advance
{
    class DrawGraphic
    {



        static Dictionary<int, double> abc = getAllQuizAverageScore();
        static public int[] quizIdInt = abc.Keys.ToArray();


        static public double[] dataX = GetDataX();
        static public double[] dataY = GetDataY();

        static public String[] quizIdString = quizIdInt.Select(x => "Quiz " + x.ToString()).ToArray();


        static double[] GetDataX()
        {
            double[] Xaxis = new double[quizIdInt.Length];
            for (int i = 0; i < quizIdInt.Length; i++)
            {
                Xaxis[i] = i * 1.0;
            }
            return Xaxis;
        }

        static double[] GetDataY()
        {
            return abc.Values.ToArray();
        }


        static Dictionary<int, double> getAllQuizAverageScore()
        {
            Dictionary<int, double> d = new Dictionary<int, double>();
            var lst = (from t in LoginWindow.ctx.Tests where t.isAvailabeToStudent == 1 select t.id).ToList();
            foreach (int i in lst)
            {
                d.Add(i, getClassAvgById(i));
            }
            foreach (var item in d.Where(kvp => kvp.Value == 0).ToList())
            {
                d.Remove(item.Key);
            }
            return d;
        }

        static double getClassAvgById(int testid)
        {

            if ((from t in LoginWindow.ctx.Answers where t.quizId == testid select t.result).Count() > 0)
                return (from t in LoginWindow.ctx.Answers where t.quizId == testid select t.result).Average();
            else
                return 0;

        }


        public static double getStdScoreByTestIdByQuizId(int testid, int stdId)
        {
            int aa = (from t in LoginWindow.ctx.Answers where (t.quizId == testid && t.studentID == stdId) select t.result).Count();
            if (aa == 1)
            {
                double resp = (from t in LoginWindow.ctx.Answers where t.quizId == testid && t.studentID == stdId select t.result).FirstOrDefault();
                return resp;
            }

            else if (aa == 0)
                return 0;
            else
            {
                MessageBox.Show("student " + stdId + " has take quiz  " + testid + " more than once.", "DataBase Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return 0;
            }
        }

    }
}
