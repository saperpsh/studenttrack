﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows;


namespace FinalProject_CS_Advance
{
    /// <summary>
    /// Interaction logic for StudentMainWindow.xaml
    /// </summary>
    public partial class StudentMainWindow : Window
    {
        Student current;
        DrawGraphic drawline = new DrawGraphic();
        public StudentMainWindow(Student student)
        {
            current = student;
            InitializeComponent();
            drawIndividualCure(current.Id);
            drawAvgClassCurve();


        }
        void drawAvgClassCurve()
        {

            wpfPlot1.plt.XTicks(labels: DrawGraphic.quizIdString);
            wpfPlot1.plt.Legend(fixedLineWidth: false, location: ScottPlot.legendLocation.upperRight);
            wpfPlot1.plt.PlotScatter(DrawGraphic.dataX, DrawGraphic.dataY, color: Color.Magenta, label: "Class Average");

            wpfPlot1.Render();

        }

        private void btTakeQuiz_Click(object sender, RoutedEventArgs e)
        {
            TakeQuiz takeQuizOrPractice = new TakeQuiz(current);
            if (takeQuizOrPractice.ShowDialog() == true)
            {
                wpfPlot1.Reset();
                drawIndividualCure(current.Id);
                drawAvgClassCurve();
                lbQuizList_Initialized(sender, e);
                lblQuizNumS_Initialized(sender, e);
            }
        }

        void drawIndividualCure(int studentId)
        {

            if (DrawGraphic.quizIdString.Length > 0)
            {
                double[] dataY = new double[DrawGraphic.quizIdString.Length];
                for (int i = 0; i < DrawGraphic.quizIdString.Length; i++)
                {
                    dataY[i] = DrawGraphic.getStdScoreByTestIdByQuizId(DrawGraphic.quizIdInt[i], studentId);
                }
                wpfPlot1.plt.XTicks(labels: DrawGraphic.quizIdString);
                wpfPlot1.plt.Legend(fixedLineWidth: false, fontSize: 6, location: ScottPlot.legendLocation.upperRight);
                wpfPlot1.plt.PlotBar(DrawGraphic.dataX, dataY, showValues: true, label: "Student", fill: false, outlineColor: Color.Blue);
                wpfPlot1.Render();
            }
        }




        private void lbQuizList_Initialized(object sender, EventArgs e)
        {
            var lst = from t in LoginWindow.ctx.Answers where t.studentID == current.Id select t;

            if (lst.Count() > 0)
            {
                lbQuizList.ItemsSource = lst.ToList();
            }

        }

        private void lblQuizNumS_Initialized(object sender, EventArgs e)
        {

            var studentHadQuizList = from answer in LoginWindow.ctx.Answers where answer.studentID == current.Id select answer.quizId;

            int getAvaibleQuizQuantity = (from tt in LoginWindow.ctx.Tests where ((tt.isAvailabeToStudent == 1) && !studentHadQuizList.Contains(tt.id)) select tt.id).Count();

            lblQuizNumS.Content = (getAvaibleQuizQuantity == 0) ? "You've no Quiz." : "You've " + getAvaibleQuizQuantity + " Quiz available!";
        }
    }
}

