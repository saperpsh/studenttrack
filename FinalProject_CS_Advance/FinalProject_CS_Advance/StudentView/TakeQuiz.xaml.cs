﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;

namespace FinalProject_CS_Advance
{
    /// <summary>
    /// Interaction logic for TakeQuizOrPractice.xaml
    /// </summary>
    
        public delegate bool CountDownHandler();

    public partial class TakeQuiz : Window
    {

        DispatcherTimer _timer;
        TimeSpan _time;


        Student current;
   
        public TakeQuiz(Student std)
        {

            current = std;
            InitializeComponent();

            gridProcessBar.Visibility = Visibility.Hidden; 

            lblID.Content = std.Id;

            //    StringReader sr = new StringReader(@"<Button xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation' 
            //Foreground='BurlyWood' FontSize='20pt'>Click Me!</Button>");

            //    XmlReader reader = XmlReader.Create(sr);

            //    Button dynamicButton = (Button)XamlReader.Load(reader);

            //    this.addGroupbox.Children.Add(dynamicButton);
            //    dynamicButton.Click += button1_Click;

        }
        double counter = 0.0;
        int quizDuration = 10;
        public void DispatcherTimerSample()
        {

            _time = TimeSpan.FromSeconds(quizDuration);          
            _timer = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, delegate
            {               
                lblTime.Text = _time.ToString("c");
                int slidervalue =(int) (counter/ quizDuration*100);
                pbStatus.Value = slidervalue;
                if (_time == TimeSpan.Zero)
                { _timer.Stop();
                    submit();
                }
                _time = _time.Add(TimeSpan.FromSeconds(-1));
                counter++;
            }, Application.Current.Dispatcher);
            _timer.Start();
        }

        



        List<string> rightAnserArray = new List<string>();
        private void btGetQuizContect_Click(object sender, RoutedEventArgs e)
        {
           
            int idx = cbQuizID.SelectedIndex;

            if (idx == -1)
            {
                MessageBox.Show(this, "Please select Quiz", "Selection error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            gridProcessBar.Visibility = Visibility.Visible;

            


            Test o = LoginWindow.ctx.Tests.FirstOrDefault(a => a.id == (int)cbQuizID.SelectedItem);

            string[] signs = { "+", "-", "*", "/" };

            List<Question> lst = new List<Question>();
            int counter = 1;
            string[] rows = o.QuestionInTXT.Split('\n');
            foreach (string row in rows)
            {

                if (row.Length != 0)
                {

                    string[] quizDetail = row.Split(';');
                    if (!int.TryParse(quizDetail[0], out int fNum))
                    {
                        MessageBox.Show(this, "Fail parse data from database", "Database data Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    if (!signs.Contains(quizDetail[1]))
                    {
                        MessageBox.Show(this, "Fail parse Operator from database", "Database data Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    string calSign = quizDetail[1];
                    if (!int.TryParse(quizDetail[2], out int sNum))
                    {
                        MessageBox.Show(this, "Fail parse data from database", "Database data Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }



                    if (!int.TryParse(quizDetail[4], out int a1))
                    {
                        MessageBox.Show(this, "Fail parse data from database", "Database data Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    if (!int.TryParse(quizDetail[5], out int a2))
                    {
                        MessageBox.Show(this, "Fail parse data from database", "Database data Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    if (!int.TryParse(quizDetail[6], out int a3))
                    {
                        MessageBox.Show(this, "Fail parse data from database", "Database data Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    if (!int.TryParse(quizDetail[7], out int a4))
                    {
                        MessageBox.Show(this, "Fail parse data from database", "Database data Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    rightAnserArray.Add(quizDetail[3]);
                    GroupBox gb = new GroupBox();
                    Thickness th = new Thickness();
                    th.Bottom = 10;
                    th.Left = 10;
                    th.Right = 10;
                    th.Top = 10;
                    gb.Name = "groupBox" + counter;
                    gb.Margin = th;
                    gb.Width = 400;
                    gb.Height = 80;
                    gb.Header = "#" + counter + ":       " + fNum + calSign + sNum + " =?";
                    StackPanel stp = new StackPanel() { Name = "stkPnl" + counter, Orientation = Orientation.Horizontal, Margin = th };

                    RadioButton rb1 = new RadioButton() { Name = "Answer" + "_1", Content = a1, Margin = th };
                    RadioButton rb2 = new RadioButton() { Name = "Answer" + "_2", Content = a2, Margin = th };
                    RadioButton rb3 = new RadioButton() { Name = "Answer" + "_3", Content = a3, Margin = th };
                    RadioButton rb4 = new RadioButton() { Name = "Answer" + "_4", Content = a4, Margin = th };


                    stp.Children.Add(rb1);
                    stp.Children.Add(rb2);
                    stp.Children.Add(rb3);
                    stp.Children.Add(rb4);


                    gb.Content = stp;


                    this.addGroupbox.Children.Add(gb);


                }
                counter++;

            }
            btGetQuizContect.IsEnabled = false;
            btSubmit.IsEnabled = true;
            DispatcherTimerSample();
        }
       

        private void cbQuizID_Initialized(object sender, EventArgs e)
        {

            var studentHadQuizList = from answer in LoginWindow.ctx.Answers where answer.studentID == current.Id select answer.quizId;

            var getQuizID = from tt in LoginWindow.ctx.Tests where ((tt.isAvailabeToStudent == 1) && !studentHadQuizList.Contains(tt.id)) select tt.id;
            if (getQuizID.Count() > 0)
                cbQuizID.ItemsSource = getQuizID.ToList();
            cbQuizID.SelectedIndex = -1;
        }

        string getResultIndex(string input)
        {
            return input.Substring(input.IndexOf('_') + 1);
        }

        private void btSubmit_Click(object sender, RoutedEventArgs e)
        {

            submit();

        }



        void submit()
        {
            List<string> answerCollection = new List<string>();

            List<StackPanel> btnList = FindChirldHelper.FindVisualChild<StackPanel>(addGroupbox);
            foreach (StackPanel item in btnList)
            {
                RadioButton checkedButton = item.Children.OfType<RadioButton>()
                                       .FirstOrDefault(r => r.IsChecked == true);
                answerCollection.Add(checkedButton == null ? "0" : getResultIndex(checkedButton.Name));

            }

            string notResp = "";
            string respondString = string.Join(";", answerCollection.ToArray());

            if (respondString.Contains("0") && (int)counter < quizDuration)
            {
                int counter = 1;
                foreach (string str in answerCollection)
                {
                    if (str.Equals("0"))
                    {
                        notResp += "Question " + counter + " has no Response.\n";
                    }
                    counter++;
                }

                MessageBoxResult result = MessageBox.Show(this, notResp, "questions not answered below:", MessageBoxButton.YesNo);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        break;
                    case MessageBoxResult.No:
                        return;

                }
            }

            double score = 0.0;

            for (int i = 0; i < answerCollection.Count; i++)
            {
                if (answerCollection[i].Equals(rightAnserArray[i]))
                {
                    score++;
                }

            }


            Answer a = new Answer()
            {
                Id = 0,
                quizId = (int)cbQuizID.SelectedItem,
                result = Math.Round(score / answerCollection.Count * 100, 2),
                AnswerCollection = respondString,
                studentID = current.Id,
                ReviewByParent = "No"
            };

            LoginWindow.ctx.Answers.Add(a);
            LoginWindow.ctx.SaveChanges();
            MessageBox.Show(this, "done", "Quiz Completed", MessageBoxButton.OK, MessageBoxImage.Information);
            DialogResult = true;
            this.Close();
        }




        public static class FindChirldHelper
        {
            public static List<T> FindVisualChild<T>(DependencyObject obj) where T : DependencyObject
            {
                try
                {
                    List<T> TList = new List<T> { };
                    for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
                    {
                        DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                        if (child != null && child is T)
                        {
                            TList.Add((T)child);
                            List<T> childOfChildren = FindVisualChild<T>(child);
                            if (childOfChildren != null)
                            {
                                TList.AddRange(childOfChildren);
                            }
                        }
                        else
                        {
                            List<T> childOfChildren = FindVisualChild<T>(child);
                            if (childOfChildren != null)
                            {
                                TList.AddRange(childOfChildren);
                            }
                        }
                    }
                    return TList;
                }
                catch (Exception ee)
                {
                    MessageBox.Show(ee.Message);
                    return null;
                }
            }
        }

        private void cbQuizID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbQuizID.SelectedIndex != -1)
                btGetQuizContect.IsEnabled = true;
            else
            {
                btGetQuizContect.IsEnabled = false;
            }
        }


    }
}
