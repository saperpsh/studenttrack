﻿using System;
using System.Linq;
using System.Windows;

namespace FinalProject_CS_Advance
{
    /// <summary>
    /// Interaction logic for TeacherPublishQuiz.xaml
    /// </summary>
    public partial class TeacherPublishQuiz : Window
    {
        public TeacherPublishQuiz()
        {
            InitializeComponent();
            getAllQuizAvailable();
        }


        //private void cbQuizID_Initialized(object sender, EventArgs e)
        //{
        //    var getQuizID = from test in LoginWindow.ctx.Tests select test.id;
        //    if (getQuizID.Count() > 0)
        //        cbQuizID.ItemsSource = getQuizID.ToList();
        //    cbQuizID.SelectedIndex = -1;
        //}

        void getAllQuizAvailable()
        {
            var lst = from o in LoginWindow.ctx.Tests where o.isAvailabeToStudent == 0 select o;
            if (lst.Count() > 0)
            {
                LvAllQuiz.ItemsSource = lst.ToList();
            }

        }
        public static DateTime Now { get; }
        private void btPublish_Click(object sender, RoutedEventArgs e)
        {
            int idx = LvAllQuiz.SelectedIndex;
            if (idx == -1)
            {
                return;
            }
            Test t = (Test)LvAllQuiz.SelectedItem;
            t.isAvailabeToStudent = 1;
            LoginWindow.ctx.SaveChanges();
            MessageBox.Show(this, "updated", "Publish the quiz", MessageBoxButton.OK, MessageBoxImage.Information);
            getAllQuizAvailable();
        }
    }

}
