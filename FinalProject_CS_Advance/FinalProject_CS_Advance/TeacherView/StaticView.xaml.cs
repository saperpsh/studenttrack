﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows;

namespace FinalProject_CS_Advance.TeacherView
{

    public partial class StaticView : Window
    {

        public StaticView()
        {
            InitializeComponent();
            drawAvgClassCurve();

        }



        void drawAvgClassCurve()
        {

            wpfPlot1.plt.XTicks(labels: DrawGraphic.quizIdString);
            wpfPlot1.plt.Legend(fixedLineWidth: false, location: ScottPlot.legendLocation.upperRight);
            wpfPlot1.plt.PlotScatter(DrawGraphic.dataX, DrawGraphic.dataY, color: Color.Magenta, label: "Class Average");
            wpfPlot1.Render();
        }





        private void cmbStudetId_Initialized(object sender, EventArgs e)
        {
            var lst = from o in LoginWindow.ctx.Students select o.Id;
            if (lst.Count() > 0)
                cmbStudetId.ItemsSource = lst.ToList();
            cmbStudetId.SelectedIndex = -1;
            flag = false;
        }
        Boolean flag = true;

        private void cmbStudetId_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (flag)
                return;


            int sudId = (int)cmbStudetId.SelectedItem;
            if (sudId > 0)
            {
                wpfPlot1.Reset();
                double[] dataY = new double[DrawGraphic.quizIdString.Length];
                for (int i = 0; i < DrawGraphic.quizIdString.Length; i++)
                {
                    dataY[i] = DrawGraphic.getStdScoreByTestIdByQuizId(DrawGraphic.quizIdInt[i], sudId);
                }


                wpfPlot1.plt.XTicks(labels: DrawGraphic.quizIdString);
                wpfPlot1.plt.Legend(fixedLineWidth: false, location: ScottPlot.legendLocation.upperRight);
                wpfPlot1.plt.PlotBar(DrawGraphic.dataX, dataY, label: "Student", fill: false, outlineColor: Color.Blue);
                wpfPlot1.Render();
                drawAvgClassCurve();
            }
        }
    }
}



