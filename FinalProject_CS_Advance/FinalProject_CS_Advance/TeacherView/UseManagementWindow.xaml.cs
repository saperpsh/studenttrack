﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace FinalProject_CS_Advance
{
    /// <summary>
    /// Interaction logic for UseManagementWindow.xaml
    /// </summary>
    public partial class UseManagementWindow : Window
    {
        Regex rgxName = new Regex(@"^[a-zA-Z]{2,20}$");
        Student _currentStudent=null;
        Parent _currentParent = null;
        public UseManagementWindow()
        {

            InitializeComponent();


        }

        void getAllParents()
        {
            var lst = from o in LoginWindow.ctx.Parents select o;

            if (lst.Count() > 0)
            {
                LVParents_UserManagement.ItemsSource = lst.ToList();
            }
        }


        void getAllStudents()
        {
            //var lst = from s in ctx.Students join p in ctx.Parents on s.parentID equals p.Id select new { Id = s.Id, FirstName = s.FirstName, LastName = s.LastName, Address = s.Address, parentName = p.LastName + " " + p.FirstName };
            var lst = from o in LoginWindow.ctx.Students select o;
            if (lst.Count() > 0)
            {
                LVStudents_UserManagement.ItemsSource = lst.ToList();
            }
        }

        



        private void btUpdate_UserManagement_Students_Click(object sender, RoutedEventArgs e)
        {
            Student select = (Student)LVStudents_UserManagement.SelectedItem;
            if (select == null)
            {
                return;
            }

            int id = select.Id;
            Student o = LoginWindow.ctx.Students.FirstOrDefault(a => a.Id == id);
            o.FirstName = tbFirstName_UserManagement_Student.Text;
            o.LastName = tbLastName_UserManagement_Student.Text;
            var bitmapSource = btImage.Source as BitmapSource;
            o.Photo = bitmapImageToByteArray((BitmapImage)bitmapSource);
            o.parentID = (int)cbBox.SelectedItem;
            o.Address = tbAddress_UserManagement_Student.Text;
            o.LoginID = tbLoginID_UserManagement_Student.Text;
            o.LoginPassword = tbloginPW_UserManagement_Student.Text;
            LoginWindow.ctx.SaveChanges();

            MessageBox.Show(this, "updated", "SQl Info", MessageBoxButton.OK, MessageBoxImage.Information);

            getAllStudents();
            LVStudents_UserManagement.Items.Refresh();

        }

        private void btAdd_UserManagement_Students_Click(object sender, RoutedEventArgs e)
        {
            int id = 0;
            string fname = tbFirstName_UserManagement_Student.Text;
            string lname = tbLastName_UserManagement_Student.Text;
            string address = tbAddress_UserManagement_Student.Text;
            string loginID = tbLoginID_UserManagement_Student.Text;

            string loginPW = tbloginPW_UserManagement_Student.Text;
            var bitmapSource = btImage.Source as BitmapSource;
            byte[] bytes = bitmapImageToByteArray((BitmapImage)bitmapSource);
            int pid;
            if (cbBox.SelectedIndex == -1)
            {
                MessageBox.Show(this, "Select Parent ID", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            if (!int.TryParse(cbBox.SelectedItem.ToString(), out pid))
            {
                MessageBox.Show(this, "Parse int fail: \n", "System error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            try
            {
                Student s = new Student() { Id = id, FirstName = fname, parentID = pid, LastName = lname, Address = address, LoginID = loginID, LoginPassword = loginPW, Photo = bytes };
                LoginWindow.ctx.Students.Add(s);
                LoginWindow.ctx.SaveChanges();

            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, "Adding can't finished: \n" + ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            MessageBox.Show(this, "added", "SQl Info", MessageBoxButton.OK, MessageBoxImage.Information);

            getAllStudents();
            LVStudents_UserManagement.Items.Refresh();
           

        }

        private void LVParents_UserManagement_Initialized(object sender, EventArgs e)
        {
            getAllParents();
            LVParents_UserManagement.SelectedIndex = -1;
        }




        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string tabItem = ((sender as TabControl).SelectedItem as TabItem).Header as string;

            switch (tabItem)
            {
                case "Parent":
                    getAllParents();
                    break;

                case "Student":
                    getAllStudents();
                    break;

                default:
                    return;
            }
        }



        private byte[] bitmapImageToByteArray(BitmapImage bitmapImage)
        {
            if (bitmapImage == null) return null;
            byte[] data;
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
            using (MemoryStream ms = new MemoryStream())
            {
                encoder.Save(ms);
                data = ms.ToArray();
            }
            return data;
        }

        private ImageSource ByteToImage(byte[] imageData)
        {
            try
            {
                BitmapImage biImg = new BitmapImage();
                MemoryStream ms = new MemoryStream(imageData);
                biImg.BeginInit();
                biImg.StreamSource = ms;
                biImg.EndInit();

                ImageSource imgSrc = biImg as ImageSource;

                return imgSrc;
            }
            catch (NotSupportedException)
            {
                return null;
            }
        }

        private void cbBox_Initialized(object sender, EventArgs e)
        {
            cbBox.ItemsSource = (from o in LoginWindow.ctx.Parents select o.Id).ToList();
        }

        private void btImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDlg = new OpenFileDialog();
            openFileDlg.Filter = "Image Files|*.jpg;*.png;*.bmp;*.gif";
            if (openFileDlg.ShowDialog() == true)
            {
                try
                {
                    btImage.Source = new BitmapImage(new Uri(openFileDlg.FileName));
                }
                catch (FileNotFoundException ex)
                {
                    MessageBox.Show(this, "Image cannot find: \n" + ex.Message, "Picture error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                catch (UriFormatException ex)
                {
                    MessageBox.Show(this, "Image cannot find: \n" + ex.Message, "Picture error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                catch (ArgumentNullException ex)
                {
                    MessageBox.Show(this, "Image cannot find: \n" + ex.Message, "Picture error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
        }




        private void LVStudents_UserManagement_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (LVStudents_UserManagement.SelectedIndex != -1)
            {
                _currentStudent = (Student)LVStudents_UserManagement.SelectedItem;

                lblID_UserManagement_Student.Content = _currentStudent.Id + "";
                tbFirstName_UserManagement_Student.Text = _currentStudent.FirstName;
                tbLastName_UserManagement_Student.Text = _currentStudent.LastName;
                tbAddress_UserManagement_Student.Text = _currentStudent.Address;
                tbLoginID_UserManagement_Student.Text = _currentStudent.LoginID;
                tbloginPW_UserManagement_Student.Text = _currentStudent.LoginPassword;
                cbBox.Text = _currentStudent.parentID + "";
                if (_currentStudent.Photo != null)
                    btImage.Source = ByteToImage(_currentStudent.Photo);
                else
                {
                    btImage.Source = null;
                }
                btUpdate_UserManagement_Students.IsEnabled = true;
                btDelete_UserManagement_Students.IsEnabled = true;
            }
            else
            {
                cbBox.SelectedIndex = -1;
                lblID_UserManagement_Student.Content = "---";
                tbFirstName_UserManagement_Student.Text = "";
                tbLastName_UserManagement_Student.Text = "";
                tbAddress_UserManagement_Student.Text = "";
                tbLoginID_UserManagement_Student.Text = "";
                tbloginPW_UserManagement_Student.Text = "";
                btImage.Source =null;
                btUpdate_UserManagement_Students.IsEnabled = false;
                btDelete_UserManagement_Students.IsEnabled = false;
            }
        }

        private void LVParents_UserManagement_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (LVParents_UserManagement.SelectedIndex != -1)
            {
                _currentParent = (Parent)LVParents_UserManagement.SelectedItem;

                lblID_UserManagement_Parent.Content = _currentParent.Id + "";
                tbFirstName_UserManagement_Parents.Text = _currentParent.FirstName;
                tbLastName_UserManagement_Parents.Text = _currentParent.LastName;
                tbPhone_UserManagement_Parents.Text = _currentParent.Phone;
                tbEmail_UserManagement_Parents.Text = _currentParent.Email;
                tbLoginID_UserManagement_Parents.Text = _currentParent.loginID;
                tbloginPW_UserManagement_Parents.Text = _currentParent.LoginPassword;
                btUpdate_UserManagement_Students.IsEnabled = true;
                btDelete_UserManagement_Students.IsEnabled = true;

            }
            else
            {
                lblID_UserManagement_Parent.Content = "---";
                tbFirstName_UserManagement_Parents.Text = "";
                tbLastName_UserManagement_Parents.Text = "";
                tbLastName_UserManagement_Parents.Text = "";
                tbEmail_UserManagement_Parents.Text = "";
                tbLoginID_UserManagement_Parents.Text = "";
                tbloginPW_UserManagement_Parents.Text = "";
                tbPhone_UserManagement_Parents.Text = "";
                btUpdate_UserManagement_Students.IsEnabled = false;
                btDelete_UserManagement_Students.IsEnabled = false;
            }

           

        }

        private void LVStudents_UserManagement_Initialized(object sender, EventArgs e)
        {
            getAllStudents();
            LVStudents_UserManagement.SelectedIndex = -1;
        }

        private void btUpdate_UserManagement_Parents_Click(object sender, RoutedEventArgs e)
        {
            Parent select = (Parent)LVParents_UserManagement.SelectedItem;
            if (select == null)
            {
                return;
            }

            int id = select.Id;
            Parent o = LoginWindow.ctx.Parents.FirstOrDefault(a => a.Id == id);
            o.FirstName = tbFirstName_UserManagement_Parents.Text;
            o.LastName = tbLastName_UserManagement_Parents.Text;
            o.Phone = tbPhone_UserManagement_Parents.Text;
            o.Email = tbEmail_UserManagement_Parents.Text;
            o.loginID = tbLoginID_UserManagement_Parents.Text;
            o.LoginPassword = tbloginPW_UserManagement_Parents.Text;
            LoginWindow.ctx.SaveChanges();

            MessageBox.Show(this, "updated", "SQl Info", MessageBoxButton.OK, MessageBoxImage.Information);
            getAllParents();
            LVParents_UserManagement.Items.Refresh();

        }


        private void btAdd_UserManagement_Parents_Click(object sender, RoutedEventArgs e)
        {
            int id = 0;
            string fname = tbFirstName_UserManagement_Parents.Text;
            string lname = tbLastName_UserManagement_Parents.Text;
            string phone = tbPhone_UserManagement_Parents.Text;
            string email = tbEmail_UserManagement_Parents.Text;
            string loginId = tbLoginID_UserManagement_Parents.Text;
            string loginPW = tbloginPW_UserManagement_Parents.Text;

            try
            {
                Parent s = new Parent() { Id = id, FirstName = fname, LastName = lname, Phone = phone, Email = email, LoginPassword = loginPW, loginID = loginId };
                LoginWindow.ctx.Parents.Add(s);
                LoginWindow.ctx.SaveChanges();

            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, "Adding can't finished: \n" + ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            MessageBox.Show(this, "added", "SQl Info", MessageBoxButton.OK, MessageBoxImage.Information);

            getAllParents();
            LVParents_UserManagement.Items.Refresh();
            cbBox.ItemsSource = (from o in LoginWindow.ctx.Parents select o.Id).ToList();


        }






        private void btDelete_UserManagement_Parents_Click(object sender, RoutedEventArgs e)
        {

            if (_currentParent == null)
            {
                MessageBox.Show(this, "Select a parent: \n", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            int ddd = (from std in LoginWindow.ctx.Students where _currentParent.Id == std.parentID select std).Count();
            if (ddd > 0)
            {
                MessageBox.Show(this, "You can't delete him: because he/she has record in Database", "Delete error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                LoginWindow.ctx.Parents.Remove(_currentParent);
                LoginWindow.ctx.SaveChanges();
                getAllParents();
                LVParents_UserManagement.Items.Refresh();
            }




        }

        private void btDelete_UserManagement_Students_Click(object sender, RoutedEventArgs e)
        {

            if (_currentStudent == null)
            {
                MessageBox.Show(this, "Select a student: \n", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            int ddd = (from answer in LoginWindow.ctx.Answers where _currentStudent.Id == answer.studentID select answer).Count();
            if (ddd > 0)
            {
                MessageBox.Show(this, "You can't delete him: because he/she has record in Database", "Delete error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                LoginWindow.ctx.Students.Remove(_currentStudent);
                LoginWindow.ctx.SaveChanges();
                getAllStudents();
                LVStudents_UserManagement.Items.Refresh();
            }

        }


    }
}
