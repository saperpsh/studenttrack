﻿using FinalProject_CS_Advance.TeacherView;
using System.Windows;

namespace FinalProject_CS_Advance
{
    /// <summary>
    /// Interaction logic for TeacherMainWindow.xaml
    /// </summary>
    public partial class TeacherMainWindow : Window
    {
        public TeacherMainWindow()
        {
            InitializeComponent();

        }



        private void bt_Teacher_DBManagement_Click(object sender, RoutedEventArgs e)
        {
            UseManagementWindow useManagementWindow = new UseManagementWindow();
            if (useManagementWindow.ShowDialog() == true)
            {

            }
        }

        private void bt_Teacher_Static_Click(object sender, RoutedEventArgs e)
        {
            StaticView staticView = new StaticView();
            if (staticView.ShowDialog() == true)
            {

            }



        }

        private void bt_Teacher_CreateQuiz_Click(object sender, RoutedEventArgs e)
        {
            CreateQuizWindow teacherStaticWindow = new CreateQuizWindow();
            if (teacherStaticWindow.ShowDialog() == true)
            {

            }
        }



        private void bt_Teacher_publicQuestions_Click(object sender, RoutedEventArgs e)
        {
            TeacherPublishQuiz createQuizWindow = new TeacherPublishQuiz();
            if (createQuizWindow.ShowDialog() == true)
            {

            }
        }
    }
}
