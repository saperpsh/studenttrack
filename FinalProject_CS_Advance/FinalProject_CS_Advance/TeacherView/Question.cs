﻿namespace FinalProject_CS_Advance
{


    class Question
    {
        public int FNumber { get; set; }
        public string Operator { get; set; }
        public int SNumber { get; set; }
        public int A1 { get; set; }
        public int A2 { get; set; }
        public int A3 { get; set; }
        public int A4 { get; set; }


        public int RightAnswerlocation { get; set; }

        public Question(int fNumber, string @operator, int sNumber, int rightAnswerlocation, int a1, int a2, int a3, int a4)
        {
            FNumber = fNumber;
            Operator = @operator;
            SNumber = sNumber;
            A1 = a1;
            A2 = a2;
            A3 = a3;
            A4 = a4;
            RightAnswerlocation = rightAnswerlocation;
        }

        public override string ToString()
        {
            return string.Format("{0};{1};{2};{3};{4};{5};{6};{7};", FNumber, Operator, SNumber, RightAnswerlocation, A1, A2, A3, A4);

        }

    }




}
