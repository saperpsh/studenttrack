﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;


namespace FinalProject_CS_Advance
{
    /// <summary>
    /// Interaction logic for CreateQuizWindow.xaml
    /// </summary>
    public partial class CreateQuizWindow : Window
    {

        public CreateQuizWindow()
        {



            InitializeComponent();


        }



        private void cbScope_Initialized(object sender, EventArgs e)
        {
            int[] calScope = { 100, 1000, 10000 };
            cbScope.ItemsSource = calScope;
        }

        private void btGenerateQuiz_Click(object sender, RoutedEventArgs e)
        {
            List<Question> lst = new List<Question>();

            if (cbScope.SelectedIndex == -1)
            {
                MessageBox.Show(this, "Please select scope", "Parameter error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            int scropeInt = (int)cbScope.SelectedItem;
            int noPlus = (int)sliderQuizPlus.Value;

            if (noPlus > 0)
            {
                for (int i = 0; i < noPlus; i++)
                {
                    int a1;
                    int a2;
                    int a3;
                    int a4;


                    int fNum = RandomNumber(scropeInt / 10, scropeInt);
                    int sNum = RandomNumber(1, scropeInt - fNum);
                    int result = fNum + sNum;
                    int rightAnswerLocation = RandomNumber(1, 5);

                    switch (rightAnswerLocation)
                    {
                        case 1:
                            a1 = result;
                            a2 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, result - 1);
                            a3 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, result - 1);
                            while (a2 == a3)
                            {
                                a3 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, result - 1);
                            }
                            a4 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, result - 1);
                            while (a4 == a3 || a4 == a2)
                            {
                                a4 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, result - 1);
                            }
                            break;
                        case 2:
                            a2 = result;
                            a1 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, result - 1);

                            a3 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, result - 1);
                            while (a1 == a3)
                            {
                                a3 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, result - 1);
                            }
                            a4 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, result - 1);
                            while (a4 == a3 || a4 == a1)
                            {
                                a4 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, result - 1);
                            }
                            break;
                        case 3:
                            a3 = result;
                            a1 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, result - 1);
                            a2 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, result - 1);
                            while (a1 == a2)
                            {
                                a2 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, result - 1);
                            }
                            a4 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, result - 1);
                            while (a4 == a2 || a4 == a1)
                            {
                                a4 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, result - 1);
                            }
                            break;
                        case 4:
                            a4 = result;
                            a2 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, result - 1);
                            a3 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, result - 1);
                            while (a3 == a2)
                            {
                                a3 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, result - 1);
                            }
                            a1 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, result - 1);
                            while (a3 == a1 || a2 == a1)
                            {
                                a1 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, result - 1);
                            }
                            break;
                        default:
                            MessageBox.Show(this, "out of 1-4 option", "System error", MessageBoxButton.OK, MessageBoxImage.Error);
                            return;

                    }

                    lst.Add(new Question(fNum, "+", sNum, rightAnswerLocation, a1, a2, a3, a4));
                }
            }

            int noSubstract = (int)sliderQuizSubstract.Value;
            if (noSubstract > 0)
            {
                for (int i = 0; i < noSubstract; i++)
                {
                    int a1;
                    int a2;
                    int a3;
                    int a4;

                    int min = Math.Max(scropeInt / 10, 2);

                    int fNum = RandomNumber(min, scropeInt);
                    int sNum = RandomNumber(0, fNum);
                    int result = fNum - sNum;
                    int rightAnswerLocation = RandomNumber(1, 5);
                    int max = Math.Max(5, result);
                    switch (rightAnswerLocation)
                    {
                        case 1:
                            a1 = result;
                            a2 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);
                            a3 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);

                            a4 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);

                            break;
                        case 2:
                            a2 = result;
                            a1 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);

                            a3 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);

                            a4 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);

                            break;
                        case 3:
                            a3 = result;
                            a1 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);
                            a2 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);

                            a4 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);

                            break;
                        case 4:
                            a4 = result;
                            a2 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);
                            a3 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);

                            a1 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);

                            break;
                        default:
                            MessageBox.Show(this, "out of 1-4 option", "System error", MessageBoxButton.OK, MessageBoxImage.Error);
                            return;

                    }

                    lst.Add(new Question(fNum, "-", sNum, rightAnswerLocation, a1, a2, a3, a4));
                }
            }
            int noMultiple = (int)sliderQuizMultiple.Value;
            if (noMultiple > 0)
            {
                for (int i = 0; i < noMultiple; i++)
                {
                    int a1;
                    int a2;
                    int a3;
                    int a4;



                    int fNum = RandomNumber(2, scropeInt / 2);
                    int sNum = (int)scropeInt / fNum;
                    int result = fNum * sNum;
                    int rightAnswerLocation = RandomNumber(1, 5);
                    int max = Math.Max(5, result);
                    switch (rightAnswerLocation)
                    {
                        case 1:
                            a1 = result;
                            a2 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);
                            a3 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);

                            a4 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);

                            break;
                        case 2:
                            a2 = result;
                            a1 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);

                            a3 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);

                            a4 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);

                            break;
                        case 3:
                            a3 = result;
                            a1 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);
                            a2 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);

                            a4 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);

                            break;
                        case 4:
                            a4 = result;
                            a2 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);
                            a3 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);

                            a1 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);

                            break;
                        default:
                            MessageBox.Show(this, "out of 1-4 option", "System error", MessageBoxButton.OK, MessageBoxImage.Error);
                            return;

                    }

                    lst.Add(new Question(fNum, "*", sNum, rightAnswerLocation, a1, a2, a3, a4));
                }
            }
            int noDivision = (int)sliderQuizDivision.Value;
            if (noDivision > 0)
            {
                for (int i = 0; i < noDivision; i++)
                {
                    int a1;
                    int a2;
                    int a3;
                    int a4;

                    KeyValuePair<int, int> val = GenerateIntDivisibleNoPair(3, scropeInt);

                    int fNum = val.Key;
                    int sNum = val.Value;
                    int result = fNum / sNum;
                    int rightAnswerLocation = RandomNumber(1, 5);
                    int max = Math.Max(5, result);
                    switch (rightAnswerLocation)
                    {
                        case 1:
                            a1 = result;
                            a2 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);
                            a3 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);

                            a4 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);

                            break;
                        case 2:
                            a2 = result;
                            a1 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);

                            a3 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);

                            a4 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);

                            break;
                        case 3:
                            a3 = result;
                            a1 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);
                            a2 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);

                            a4 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);

                            break;
                        case 4:
                            a4 = result;
                            a2 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);
                            a3 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);

                            a1 = result + (int)Math.Pow(-1, random.Next(10) % 2) * RandomNumber(1, max);

                            break;
                        default:
                            MessageBox.Show(this, "out of 1-4 option", "System error", MessageBoxButton.OK, MessageBoxImage.Error);
                            return;

                    }

                    lst.Add(new Question(fNum, "/", sNum, rightAnswerLocation, a1, a2, a3, a4));
                }
            }

            DVQuizDetail.ItemsSource = lst;

        }

        static int GetRandomSeed()
        {
            byte[] bytes = new byte[4];
            System.Security.Cryptography.RNGCryptoServiceProvider rng = new System.Security.Cryptography.RNGCryptoServiceProvider();
            rng.GetBytes(bytes);
            return BitConverter.ToInt32(bytes, 0);

        }
        static Random random = new Random(GetRandomSeed());
        public int RandomNumber(int min, int max)
        {

            return random.Next(min, max);
        }



        private void btSaveQuiz_Click(object sender, RoutedEventArgs e)
        {


            string txt = "";
            foreach (Question p in DVQuizDetail.Items)
            {
                txt += p.ToString();
                txt += "\n";
            }
            DateTime dt;

            DateTime today = new DateTime();
            try
            {
                dt = datePicker_CreateQuizWindow.SelectedDate.Value;
            }
            catch (InvalidOperationException)
            {
                MessageBox.Show(this, "Input valid Quiz date", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (dt < today)
            {
                MessageBox.Show(this, "Input valid Quiz date", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Test t = new Test()
            {
                id = 0,
                date = dt,
                QuestionInTXT = txt,
                isAvailabeToStudent = 0
            };
            LoginWindow.ctx.Tests.Add(t);
            LoginWindow.ctx.SaveChanges();
            Test last = LoginWindow.ctx.Tests
                       .OrderByDescending(p => p.id)
                       .FirstOrDefault();

            MessageBox.Show(this, "Quiz " + last.id + " Created ", "Quiz Created", MessageBoxButton.OK, MessageBoxImage.Information);

            DVQuizDetail.ItemsSource = null;
            refresh();



        }

        private void refresh()
        {
            var lst = from o in LoginWindow.ctx.Tests select o.id;
            if (lst.Count() > 0)
                cbQuizID.ItemsSource = lst.ToList();
            cbQuizID.SelectedIndex = -1;

        }


        static string[] operatorsArray = { "+", "-", "*", "/" };

        private void DVQuizDetail_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1).ToString();
        }
        public static bool IsPrime(int number)
        {
            if (number <= 0) return false;
            if (number == 2 || number == 1) return true;
            if (number % 2 == 0) return false;

            var boundary = (int)Math.Floor(Math.Sqrt(number));

            for (int i = 3; i <= boundary; i += 2)
                if (number % i == 0)
                    return false;

            return true;
        }


        public KeyValuePair<int, int> GenerateIntDivisibleNoPair(int p, int q)
        {
            if (p <= 0 || q <= 0 || q <= p)
                throw new ArgumentException(); //for simplification of idea



            int a = random.Next(p, q);
            while (IsPrime(a))
            {
                a = random.Next(p, q);
            }
            int d = 0;
            for (int i = 2; i <= (int)Math.Floor(Math.Sqrt(a)); i++)
            {
                if (a % i == 0)
                    d = i;
            }



            return new KeyValuePair<int, int>(a, d);
        }

        Boolean flag = true;

        private void cbQuizID_Initialized(object sender, EventArgs e)
        {
            refresh();
            flag = false;

        }

        private void cbQuizID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (flag)
            {

                return;
            }

            int idx = cbQuizID.SelectedIndex;
            if (idx != -1)
            {
                loadDataFromTestByQuizID((int)cbQuizID.SelectedItem);
            }

        }

        private void loadDataFromTestByQuizID(int cbID)

        {


            int a1;
            int a2;
            int a3;
            int a4;

            string[] signs = { "+", "-", "*", "/" };
            int fNum;

            int sNum;

            int rightAnswerLocation;
            Test o = LoginWindow.ctx.Tests.FirstOrDefault(a => a.id == cbID);
            datePicker_CreateQuizWindow.SelectedDate = o.date;
            List<Question> lst = new List<Question>();

            string[] rows = o.QuestionInTXT.Split('\n');

            foreach (string row in rows)
            {

                if (row.Length != 0)
                {
                    string[] quizDetail = row.Split(';');


                    if (!int.TryParse(quizDetail[0], out fNum))
                    {
                        MessageBox.Show(this, "Fail parse data from database", "Database data Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }



                    if (!signs.Contains(quizDetail[1]))
                    {
                        MessageBox.Show(this, "Fail parse Operator from database", "Database data Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                    string calSign = quizDetail[1];


                    if (!int.TryParse(quizDetail[2], out sNum))
                    {
                        MessageBox.Show(this, "Fail parse data from database", "Database data Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                    if (!int.TryParse(quizDetail[3], out rightAnswerLocation))
                    {
                        MessageBox.Show(this, "Fail parse data from database", "Database data Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                    if (!int.TryParse(quizDetail[4], out a1))
                    {
                        MessageBox.Show(this, "Fail parse data from database", "Database data Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    if (!int.TryParse(quizDetail[5], out a2))
                    {
                        MessageBox.Show(this, "Fail parse data from database", "Database data Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    if (!int.TryParse(quizDetail[6], out a3))
                    {
                        MessageBox.Show(this, "Fail parse data from database", "Database data Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                    if (!int.TryParse(quizDetail[7], out a4))
                    {
                        MessageBox.Show(this, "Fail parse data from database", "Database data Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                    lst.Add(new Question(fNum, calSign, sNum, rightAnswerLocation, a1, a2, a3, a4));

                }

            }


            DVQuizDetail.ItemsSource = lst;
        }

        private void MenuItem_Checked(object sender, RoutedEventArgs e)
        {
            int id = DVQuizDetail.SelectedIndex;

        }
    }
}
